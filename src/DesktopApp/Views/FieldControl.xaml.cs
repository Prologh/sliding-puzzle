﻿using SlidingPuzzle.DesktopApp.ViewModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace SlidingPuzzle.DesktopApp.Views
{
    /// <summary>
    /// Interaction logic for FieldControl.xaml
    /// </summary>
    public partial class FieldControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldControl"/> class.
        /// </summary>
        public FieldControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button))
            {
                throw new InvalidOperationException();
            }

            if (!(button.DataContext is FieldVM field))
            {
                throw new InvalidOperationException();
            }

            var fromLeft = field.Left;
            var toLeft = fromLeft + 10 + 92.5;

            var canvasLeftAnimation = new DoubleAnimation
            {
                From = fromLeft,
                To = toLeft,
                EasingFunction = new SineEase { EasingMode = EasingMode.EaseInOut },
                Duration = new Duration(TimeSpan.FromMilliseconds(400)),
            };

            //var canvasReversedLeftAnimation = new DoubleAnimation
            //{
            //    From = toLeft,
            //    To = fromLeft,
            //    EasingFunction = new SineEase { EasingMode = EasingMode.EaseInOut },
            //    Duration = new Duration(TimeSpan.FromMilliseconds(400)),
            //    BeginTime = TimeSpan.FromMilliseconds(600),
            //};

            //set the target of the animation
            Storyboard.SetTarget(canvasLeftAnimation, VisualTreeHelper.GetParent(this));
            //Storyboard.SetTarget(canvasReversedLeftAnimation, VisualTreeHelper.GetParent(this));

            //set the target property of the animation. Don't forget the ( ) around canvas.left
            Storyboard.SetTargetProperty(canvasLeftAnimation, new PropertyPath("(Canvas.Left)"));
            //Storyboard.SetTargetProperty(canvasReversedLeftAnimation, new PropertyPath("(Canvas.Left)"));

            //create the storyboard
            Storyboard myMovementStoryboard = new Storyboard();

            //add the animation to the storyboard
            myMovementStoryboard.Children.Add(canvasLeftAnimation);
            //myMovementStoryboard.Children.Add(canvasReversedLeftAnimation);

            //begin animation
            myMovementStoryboard.Begin();
        }
    }
}
