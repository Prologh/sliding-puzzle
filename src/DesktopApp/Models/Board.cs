﻿using SlidingPuzzle.DesktopApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;

namespace SlidingPuzzle.DesktopApp.Models
{
    /// <summary>
    /// Represents game board.
    /// </summary>
    [DebuggerDisplay("[{Columns} x {Rows}] Fields = {FieldsCount}")]
    public class Board
    {
        private const double FieldMargin = 10;
        private const double FieldHeight = 92.5d;
        private const double FieldWidth = 92.5d;

        /// <summary>
        /// The amount of columns.
        /// </summary>
        public static int Columns => (int)Math.Pow(PositionsCount, 0.5);

        /// <summary>
        /// Amount of empty fields.
        /// </summary>
        public static int EmptyFieldsCount => 1;

        /// <summary>
        /// Amount of fields.
        /// </summary>
        public static int FieldsCount => PositionsCount - EmptyFieldsCount;

        /// <summary>
        /// Amount of positions.
        /// </summary>
        public static int PositionsCount => 16;

        /// <summary>
        /// The amount of rows.
        /// </summary>
        public static int Rows => (int)Math.Pow(PositionsCount, 0.5);

        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/> class.
        /// </summary>
        public Board()
        {
            var shapeList = new List<Rect>();
            var fieldCollection = new ObservableCollection<FieldVM>();

            for (int y = 0; y < Rows; y++)
            {
                for (int x = 0; x < Columns; x++)
                {
                    if (fieldCollection.Count >= FieldsCount)
                    {
                        break;
                    }

                    var startingPoint = new Point
                    (
                        x: FieldMargin * (x + 1) + (FieldWidth * x),
                        y: FieldMargin * (y + 1) + (FieldHeight * y)
                    );
                    var fieldSize = new Size(FieldWidth, FieldHeight);
                    var fieldShape = new Rect(startingPoint, fieldSize);
                    shapeList.Add(fieldShape);

                    var field = new FieldVM(this, fieldShape, content: x + 1 + y * Rows, id: x + 1 + y * Rows);
                    fieldCollection.Add(field);
                }
            }

            Shapes = shapeList;
            Fields = new ReadOnlyObservableCollection<FieldVM>(fieldCollection);
        }

        /// <summary>
        /// Gets the dictionary of fields.
        /// </summary>
        public ReadOnlyObservableCollection<FieldVM> Fields { get; }

        /// <summary>
        /// Gets the list of field geomerties.
        /// </summary>
        public IReadOnlyList<Rect> Shapes { get; }
    }
}
