﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SlidingPuzzle.DesktopApp.Models
{
    /// <summary>
    /// Represents position model.
    /// </summary>
    [DebuggerDisplay("{Id}")]
    public class Position : IEquatable<Position>
    {
        /// <summary>
        /// The unset position.
        /// </summary>
        public static Position Unset => new Position(id: 0);

        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class
        /// with identifier specified.
        /// </summary>
        /// <param name="id">
        /// The identifier for current position.
        /// </param>
        public Position(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Gets the position identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current position
        /// has not yet been set.
        /// </summary>
        public bool IsUnset => Equals(Unset);

        /// <summary>
        /// Determines whether value of the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Position"/> to compare with the current <see cref="Position"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Position other)
        {
            return other != null &&
                   Id == other.Id;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Position))
            {
                return false;
            }

            return Equals((Position)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current position.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current position.
        /// </returns>
        public override string ToString()
        {
            return $"[{Id}]";
        }

        public static bool operator ==(Position position1, Position position2)
        {
            return EqualityComparer<Position>.Default.Equals(position1, position2);
        }

        public static bool operator !=(Position position1, Position position2)
        {
            return !(position1 == position2);
        }
    }
}
