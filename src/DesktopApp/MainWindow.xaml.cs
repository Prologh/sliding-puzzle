﻿using SlidingPuzzle.DesktopApp.Models;
using System.Windows;

namespace SlidingPuzzle.DesktopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new Board();
        }
    }
}
