﻿using SlidingPuzzle.DesktopApp.Models;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;

namespace SlidingPuzzle.DesktopApp.ViewModel
{
    /// <summary>
    /// Represents field view model.
    /// </summary>
    [DebuggerDisplay("#{Content} : {Shape.Location} [{Shape.Size}]")]
    public class FieldVM : INotifyPropertyChanged
    {
        private object _content;
        private Brush _foreground;
        private double _height;
        private double _left;
        //private Point _location;
        //private Size _size;
        //private Rect _shape;
        private double _top;
        private double _width;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldVM"/> class.
        /// </summary>
        /// <param name="board">
        /// The game board to which this field belongs to.
        /// </param>
        /// <param name="shape">
        /// The field initial geometric shape.
        /// </param>
        /// <param name="content">
        /// The field's content.
        /// </param>
        public FieldVM(Board board, Rect shape, object content, int id)
        {
            Board = board ?? throw new ArgumentNullException(nameof(board));
            Content = content;
            Foreground = new SolidColorBrush(Colors.White);
            Height = shape.Height;
            Id = id;
            Left = shape.Left;
            //Location = shape.Location;
            //Size = shape.Size;
            //Shape = shape;
            Top = shape.Top;
            Width = shape.Width;
        }

        /// <summary>
        /// Gets the game board to which this field belongs to.
        /// </summary>
        public Board Board { get; }

        /// <summary>
        /// Gets or sets field content.
        /// </summary>
        public object Content
        {
            get { return _content; }

            set
            {
                if (Content != value)
                {
                    _content = value;
                    RaisePropertyChanged(nameof(Content));
                }
            }
        }

        /// <summary>
        /// Gets or sets field foreground brush.
        /// </summary>
        public Brush Foreground
        {
            get { return _foreground; }

            set
            {
                if (Foreground != value)
                {
                    _foreground = value;
                    RaisePropertyChanged(nameof(Foreground));
                }
            }
        }

        /// <summary>
        /// Gets or sets field height.
        /// </summary>
        public double Height
        {
            get { return _height; }

            set
            {
                if (Height != value)
                {
                    _height = value;
                    RaisePropertyChanged(nameof(Height));
                }
            }
        }

        /// <summary>
        /// Gets the field identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets or sets field left bound.
        /// </summary>
        public double Left
        {
            get { return _left; }

            set
            {
                if (Left != value)
                {
                    _left = value;
                    RaisePropertyChanged(nameof(Left));
                }
            }
        }

        /// <summary>
        /// Gets the field location.
        /// </summary>
        public Point Location => new Point(Left, Top);

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the field size.
        /// </summary>
        public Size Size => new Size(Width, Height);

        /// <summary>
        /// Gets or sets field shape.
        /// </summary>
        public Rect Shape => new Rect(Location, Size);

        /// <summary>
        /// Gets or sets field top bound.
        /// </summary>
        public double Top
        {
            get { return _top; }

            set
            {
                if (Top != value)
                {
                    _top = value;
                    RaisePropertyChanged(nameof(Top));
                }
            }
        }

        /// <summary>
        /// Gets or sets field height.
        /// </summary>
        public double Width
        {
            get { return _width; }

            set
            {
                if (Width != value)
                {
                    _width = value;
                    RaisePropertyChanged(nameof(Width));
                }
            }
        }

        /// <summary>
        /// Invokes <see cref="PropertyChangedEventHandler"/> for specified
        /// property.
        /// </summary>
        /// <param name="propertyName">
        /// The name of changed property.
        /// </param>
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
