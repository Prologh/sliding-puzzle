﻿using SlidingPuzzle.Solver.Models;

namespace SlidingPuzzle.Solver.Heurisitcs.Abstractions
{
    public interface IHeuristicEvaluator
    {
        string Name { get; }
        int EvaluateDistance(Board board1, Board board2);
        int EvaluateDistance(Tile tile1, Tile tile2);
        int EvaluateDistance(Position position1, Position position2);
        int EvaluateDistance(Node node1, Node node2);
    }
}
