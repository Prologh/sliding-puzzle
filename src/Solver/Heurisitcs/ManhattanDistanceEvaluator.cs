﻿using SlidingPuzzle.Solver.Heurisitcs.Abstractions;
using SlidingPuzzle.Solver.Models;
using System;
using System.Linq;

namespace SlidingPuzzle.Solver.Heurisitcs
{
    public class ManhattanDistanceEvaluator : IHeuristicEvaluator
    {
        public virtual string Name => "The Manhattan Distance";

        public virtual int EvaluateDistance(Board currentBoard, Board targetBoard)
        {
            if (currentBoard == null)
            {
                throw new ArgumentNullException(nameof(currentBoard));
            }

            if (targetBoard == null)
            {
                throw new ArgumentNullException(nameof(targetBoard));
            }

            var sum = 0;
            for (int i = 0; i < currentBoard.Tiles.Count; i++)
            {
                var currentTile = currentBoard.Tiles[i];
                var targetTile = targetBoard.Tiles.First(tile => tile.Id == currentTile.Id);
                if (currentTile.IsEmpty || targetTile.IsEmpty)
                {
                    continue;
                }

                sum += EvaluateDistance(currentTile.Position, targetTile.Position);
            }

            return sum;
        }

        public virtual int EvaluateDistance(Tile tile1, Tile tile2)
        {
            if (tile1 == null)
            {
                throw new ArgumentNullException(nameof(tile1));
            }

            if (tile2 == null)
            {
                throw new ArgumentNullException(nameof(tile2));
            }

            return EvaluateDistance(tile1.Position, tile2.Position);
        }

        public virtual int EvaluateDistance(Position position1, Position position2)
        {
            return Math.Abs(position1.X - position2.X) + Math.Abs(position1.Y - position2.Y);
        }

        public virtual int EvaluateDistance(Node node1, Node node2)
        {
            if (node1 == null)
            {
                throw new ArgumentNullException(nameof(node1));
            }

            if (node2 == null)
            {
                throw new ArgumentNullException(nameof(node2));
            }

            return EvaluateDistance(node1.Board, node2.Board);
        }
    }
}
