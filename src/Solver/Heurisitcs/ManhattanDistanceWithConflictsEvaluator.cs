﻿using SlidingPuzzle.Solver.Models;
using System;
using System.Linq;

namespace SlidingPuzzle.Solver.Heurisitcs
{
    public class ManhattanDistanceWithConflictsEvaluator : ManhattanDistanceEvaluator
    {
        public override string Name => "The Manhattan Distance with linear conflicts";

        public override int EvaluateDistance(Board currentBoard, Board targetBoard)
        {
            if (currentBoard == null)
            {
                throw new ArgumentNullException(nameof(currentBoard));
            }

            if (targetBoard == null)
            {
                throw new ArgumentNullException(nameof(targetBoard));
            }

            int sum = 0, conflicts = 0;
            for (int i = 0; i < currentBoard.Tiles.Count; i++)
            {
                // Calculate the distance only for not empty tiles.
                var currentTile = currentBoard.Tiles[i];
                var targetTile = targetBoard.Tiles.First(tile => tile.Id == currentTile.Id);
                if (currentTile.IsEmpty || targetTile.IsEmpty)
                {
                    continue;
                }

                sum += EvaluateDistance(currentTile.Position, targetTile.Position);

                // Check if current tile has a conflicting tile.
                for (int j = 0; j < currentBoard.Tiles.Count; j++)
                {
                    var otherTile = currentBoard.Tiles[j];

                    // Ignore scenario where currentTile == otherTile.
                    if (i == j)
                    {
                        continue;
                    }

                    // Find target position for current tile.
                    var currentTileTargetPosition = targetBoard.Tiles.First(tile => tile.Id == currentTile.Id).Position;
                    if (currentTileTargetPosition != otherTile.Position)
                    {
                        continue;
                    }

                    // Find target position for other tile.
                    var otherTileTargetPosition = targetBoard.Tiles.First(tile => tile.Id == otherTile.Id).Position;
                    if (otherTileTargetPosition != currentTile.Position)
                    {
                        continue;
                    }

                    conflicts++;

                    // One conflict found, there is no more. End the loop.
                    break;
                }
            }

            return sum + conflicts;
        }
    }
}
