﻿using SlidingPuzzle.Solver.Heurisitcs.Abstractions;
using SlidingPuzzle.Solver.Models;
using System;
using System.Linq;

namespace SlidingPuzzle.Solver.Heurisitcs
{
    public class HammingDistanceEvaluator : IHeuristicEvaluator
    {
        public string Name => "The Hamming Distance";

        public int EvaluateDistance(Board currentBoard, Board targetBoard)
        {
            if (currentBoard == null)
            {
                throw new ArgumentNullException(nameof(currentBoard));
            }

            if (targetBoard == null)
            {
                throw new ArgumentNullException(nameof(targetBoard));
            }

            var sum = 0;
            for (int i = 0; i < currentBoard.Tiles.Count; i++)
            {
                var currentTile = currentBoard.Tiles[i];
                var targetTile = targetBoard.Tiles.First(tile => tile.Id == currentTile.Id);
                if (currentTile.IsEmpty || targetTile.IsEmpty)
                {
                    continue;
                }

                sum += EvaluateDistance(currentTile.Position, targetTile.Position);
            }

            return sum;
        }

        public int EvaluateDistance(Tile tile1, Tile tile2)
        {
            if (tile1 == null)
            {
                throw new ArgumentNullException(nameof(tile1));
            }

            if (tile2 == null)
            {
                throw new ArgumentNullException(nameof(tile2));
            }

            return EvaluateDistance(tile1.Position, tile2.Position);
        }

        public int EvaluateDistance(Position position1, Position position2)
        {
            return position1.Equals(position2) ? 0 : 1;
        }

        public int EvaluateDistance(Node node1, Node node2)
        {
            if (node1 == null)
            {
                throw new ArgumentNullException(nameof(node1));
            }

            if (node2 == null)
            {
                throw new ArgumentNullException(nameof(node2));
            }

            return EvaluateDistance(node1.Board, node2.Board);
        }
    }
}
