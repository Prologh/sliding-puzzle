﻿using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;

namespace SlidingPuzzle.Solver.Comparers
{
    public class BoardEqualityComparer : EqualityComparer<Board>
    {
        public override bool Equals(Board x, Board y)
        {
            if (x == null)
            {
                throw new ArgumentNullException(nameof(x));
            }

            if (y == null)
            {
                throw new ArgumentNullException(nameof(y));
            }

            return x.Equals(y);
        }

        public override int GetHashCode(Board obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return obj.GetHashCode();
        }
    }
}
