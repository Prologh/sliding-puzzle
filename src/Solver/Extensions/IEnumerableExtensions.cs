﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SlidingPuzzle.Solver.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Except<T>(this IEnumerable<T> collection, T item)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            return collection.Except(new T[] { item });
        }
    }
}
