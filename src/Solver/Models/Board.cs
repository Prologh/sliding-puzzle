﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SlidingPuzzle.Solver.Models
{
    [DebuggerDisplay("{this,nq}")]
    public class Board : IEquatable<Board>
    {
        private readonly Tile[,] _tileArray;
        private int? _hashCode;

        public Board(Tile[,] tileArray)
        {
            _tileArray = tileArray ?? throw new ArgumentNullException(nameof(tileArray));

            Tiles = BuildTileList(tileArray).AsReadOnly();
            EmptyTile = Tiles.FirstOrDefault(tile => tile.IsEmpty);

            if (EmptyTile == null)
            {
                throw new ArgumentException(
                    nameof(tileArray),
                    $"{nameof(tileArray)} must contain at lest one empty tile.");
            }
        }

        public Tile this[int y, int x] => _tileArray[y, x];

        public int Columns => _tileArray.GetLength(0);
        public Tile EmptyTile { get; }
        public IReadOnlyList<Tile> Tiles { get; }
        public int Rows => _tileArray.GetLength(1);

        public bool Equals(Board other)
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            var result = other != null
                && Rows == other.Rows
                && Columns == other.Columns
                && Tiles.Count == other.Tiles.Count
                && Tiles.SequenceEqual(other.Tiles);

            return result;
        }

        public Board SwapTiles(Tile tile1, Tile tile2)
        {
            if (tile1 == null)
            {
                throw new ArgumentNullException(nameof(tile1));
            }

            if (tile2 == null)
            {
                throw new ArgumentNullException(nameof(tile2));
            }

            var tileArray = new Tile[Columns, Rows];

            for (int x = 0; x < Rows; x++)
            {
                for (int y = 0; y < Columns; y++)
                {
                    var tile = _tileArray[y, x];

                    if (tile.Position == tile1.Position)
                    {
                        // Apply the second tile at the first tile position.
                        tile = new Tile(tile2.Id, tile1.Position, tile2.IsEmpty);
                    }
                    else
                    {
                        if (tile.Position == tile2.Position)
                        {
                            // Apply the first tile at the second tile position.
                            tile = new Tile(tile1.Id, tile2.Position, tile1.IsEmpty);
                        }
                    }

                    tileArray[y, x] = tile;
                }
            }

            return new Board(tileArray);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Board);
        }

        public override int GetHashCode()
        {
            if (_hashCode.HasValue)
            {
                return _hashCode.Value;
            }

            var code = 0;
            foreach (var tile in Tiles)
            {
                code ^= tile.GetHashCode();
            }

            _hashCode = code;

            return code;
        }

        public override string ToString()
        {
            return $"[{Columns} x {Rows}] Empty tile at {EmptyTile.Position}";
        }

        private List<Tile> BuildTileList(Tile[,] tileArray)
        {
            var columns = tileArray.GetLength(0);
            var rows = tileArray.GetLength(1);
            var list = new List<Tile>(columns * rows);
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    list.Add(tileArray[x, y]);
                }
            }

            return list;
        }

        public static bool operator ==(Board board1, Board board2)
        {
            return EqualityComparer<Board>.Default.Equals(board1, board2);
        }

        public static bool operator !=(Board board1, Board board2)
        {
            return !(board1 == board2);
        }
    }
}
