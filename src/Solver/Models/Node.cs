﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SlidingPuzzle.Solver.Models
{
    [DebuggerDisplay("{this,nq}")]
    public class Node : IEquatable<Node>
    {
        public Node(Board board)
        {
            Board = board ?? throw new ArgumentNullException(nameof(board));
        }

        public Node(Board board, Node parent)
        {
            Board = board ?? throw new ArgumentNullException(nameof(board));
            Parent = parent;
        }

        public Board Board { get; }
        public int FScore { get; set; }
        public int HScore { get; set; }
        public int GScore { get; set; }
        public bool HasParent => Parent != null;
        public Node Parent { get; }

        public bool Equals(Node other)
        {
            return other != null &&
                   HasParent == other.HasParent &&
                   FScore == other.FScore &&
                   HScore == other.HScore &&
                   GScore == other.GScore &&
                   EqualityComparer<Board>.Default.Equals(Board, other.Board) &&
                   EqualityComparer<Node>.Default.Equals(Parent, other.Parent);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Node);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = -857101877;
            hashCode = hashCode * -1521134295 + EqualityComparer<Board>.Default.GetHashCode(Board);
            hashCode = hashCode * -1521134295 + FScore.GetHashCode();
            hashCode = hashCode * -1521134295 + HScore.GetHashCode();
            hashCode = hashCode * -1521134295 + GScore.GetHashCode();
            hashCode = hashCode * -1521134295 + HasParent.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Node>.Default.GetHashCode(Parent);

            return hashCode;
        }

        public override string ToString()
        {
            return $"{GScore}, {Board}";
        }

        public static bool operator ==(Node node1, Node node2)
        {
            return EqualityComparer<Node>.Default.Equals(node1, node2);
        }

        public static bool operator !=(Node node1, Node node2)
        {
            return !(node1 == node2);
        }


    }
}
