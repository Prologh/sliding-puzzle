﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SlidingPuzzle.Solver.Models
{
    [DebuggerDisplay("{this,nq}")]
    public class Tile : IEquatable<Tile>
    {
        public Tile(int id, Position position)
        {
            Id = id;
            Position = position;
        }

        public Tile(int id, Position position, bool isEmpty)
        {
            Id = id;
            Position = position;
            IsEmpty = isEmpty;
        }

        public int Id { get; }
        public bool IsEmpty { get; }
        public Position Position { get; }

        public bool Equals(Tile other)
        {
            return other != null &&
                   Id == other.Id &&
                   Position.Equals(other.Position) &&
                   IsEmpty == other.IsEmpty;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Tile);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = 1790769432;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + IsEmpty.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Position>.Default.GetHashCode(Position);

            return hashCode;
        }

        public override string ToString()
        {
            return $"#{Id} {Position}{(IsEmpty ? " Empty" : string.Empty)}";
        }

        public static bool operator ==(Tile tile1, Tile tile2)
        {
            return EqualityComparer<Tile>.Default.Equals(tile1, tile2);
        }

        public static bool operator !=(Tile tile1, Tile tile2)
        {
            return !(tile1 == tile2);
        }
    }
}
