﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SlidingPuzzle.Solver.Models
{
    /// <summary>
    /// Represents position on a 2D grid.
    /// </summary>
    [DebuggerDisplay("{this,nq}")]
    public struct Position : IEquatable<Position>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class
        /// specified X and Y coordinates.
        /// </summary>
        /// <param name="x">
        /// The X-axis coordinate.
        /// </param>
        /// <param name="y">
        /// The Y-axis coordinate.
        /// </param>
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Gets the Position identifier.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// Gets the Position identifier.
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// Creates a new position by adding the axes values from other position.
        /// </summary>
        /// <param name="translationPosition">
        /// The other position.
        /// </param>
        /// <returns>
        /// An instance of <see cref="Position"/>.
        /// </returns>
        public Position AddTranslation(Position translationPosition)
        {
            return new Position(X + translationPosition.X, Y + translationPosition.Y);
        }

        /// <summary>
        /// Determines whether value of the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Position"/> to compare with the current <see cref="Position"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Position"/>
        /// is equal to the current <see cref="Position"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Position other)
        {
            return X.Equals(other.X)
                && Y.Equals(other.Y);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Position))
            {
                return false;
            }

            return Equals((Position)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();

            return hashCode;
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current Position.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current Position.
        /// </returns>
        public override string ToString()
        {
            return $"[{X}; {Y}]";
        }

        public static bool operator ==(Position Position1, Position Position2)
        {
            return EqualityComparer<Position>.Default.Equals(Position1, Position2);
        }

        public static bool operator !=(Position Position1, Position Position2)
        {
            return !(Position1 == Position2);
        }

        public static Position operator +(Position Position1, Position Position2)
        {
            return Position1.AddTranslation(Position2);
        }
    }
}
