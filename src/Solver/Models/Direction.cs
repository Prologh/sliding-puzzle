﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SlidingPuzzle.Solver.Models
{
    /// <summary>
    /// Indicates the direction.
    /// </summary>
    [DebuggerDisplay("{this,nq}")]
    public class Direction : IEquatable<Direction>
    {
        /// <summary>
        /// Gets the north direction.
        /// </summary>
        public static Direction North => new Direction(nameof(North), 0, -1);

        /// <summary>
        /// Gets the east direction.
        /// </summary>
        public static Direction East => new Direction(nameof(East), 1, 0);

        /// <summary>
        /// Gets the south direction.
        /// </summary>
        public static Direction South => new Direction(nameof(South), 0, 1);

        /// <summary>
        /// Gets the west direction.
        /// </summary>
        public static Direction West => new Direction(nameof(West), -1, 0);

        /// <summary>
        /// Gets the unset direction.
        /// </summary>
        public static Direction Unset => new Direction(nameof(Unset), 0, 0);

        private Direction(string name, int translateX, int translateY)
        {
            Name = name;
            Translation = new Position(translateX, translateY);
        }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current direction
        /// in unset.
        /// </summary>
        public bool IsUnset => Equals(Unset);

        /// <summary>
        /// Gets the direction name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the translation position.
        /// </summary>
        public Position Translation { get; }

        /// <summary>
        /// Determines whether value of the specified <see cref="Direction"/>
        /// is equal to the current <see cref="Direction"/>.
        /// </summary>
        /// <param name="other">
        /// The <see cref="Direction"/> to compare with the current <see cref="Direction"/>.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="Direction"/>
        /// is equal to the current <see cref="Direction"/>;
        /// otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(Direction other)
        {
            return other != null &&
                   Translation.Equals(other.Translation);
        }

        /// <summary>
        /// Returns opposite direction in relation to the current one.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="Direction"/>.
        /// </returns>
        public Direction GetOppositeDirection()
        {
            if (Equals(North))
            {
                return South;
            }

            if (Equals(South))
            {
                return North;
            }

            if (Equals(East))
            {
                return West;
            }

            if (Equals(West))
            {
                return East;
            }

            throw new InvalidOperationException("Could not find an opposite direction.");
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare with the current object.
        /// </param>
        /// <returns>
        /// <see langword="true"/> if the specified object is equal
        /// to the current object; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as Direction);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer hash code.
        /// </returns>
        public override int GetHashCode()
        {
            return -1087992660 + EqualityComparer<Position>.Default.GetHashCode(Translation);
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the current direction.
        /// </summary>
        /// <returns>
        /// <see cref="string"/> representation of current direction.
        /// </returns>
        public override string ToString()
        {
            return $"{Name} {Translation}";
        }

        public static bool operator ==(Direction direction1, Direction direction2)
        {
            return EqualityComparer<Direction>.Default.Equals(direction1, direction2);
        }

        public static bool operator !=(Direction direction1, Direction direction2)
        {
            return !(direction1 == direction2);
        }
    }
}
