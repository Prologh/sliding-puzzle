﻿using SlidingPuzzle.Solver.Collections.Generic;
using SlidingPuzzle.Solver.Heurisitcs.Abstractions;
using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;

namespace SlidingPuzzle.Solver.Services
{
    public class AStar
    {
        private readonly IHeuristicEvaluator _heuristicEvaluator;
        private readonly HeuristicNeighborNodesResolver _nodesResolver;
        private readonly NodeEqualityComparer _nodeEqualityComparer;

        public AStar(IHeuristicEvaluator heuristicEvaluator)
        {
            _heuristicEvaluator = heuristicEvaluator ?? throw new ArgumentNullException(nameof(heuristicEvaluator));
            _nodesResolver = new HeuristicNeighborNodesResolver(_heuristicEvaluator);
            _nodeEqualityComparer = new NodeEqualityComparer();
        }

        public IList<Node> FindPath(Board startingBoard, Board solutionBoard)
        {
            #region null checks
            if (startingBoard == null)
            {
                throw new ArgumentNullException(nameof(startingBoard));
            }

            if (solutionBoard == null)
            {
                throw new ArgumentNullException(nameof(solutionBoard));
            }
            #endregion

            // The set of board already evaluated.
            var closedList = new HashSet<Node>(comparer: _nodeEqualityComparer);

            var startingNode = new Node(startingBoard);
            startingNode.GScore = 0;
            startingNode.HScore = _heuristicEvaluator.EvaluateDistance(startingNode.Board, solutionBoard);
            startingNode.FScore = startingNode.GScore + startingNode.HScore;

            // The set of currently discovered nodes that are not evaluated yet.
            // Initially, only the start node is known.
            var openList = new OrderedDictionary<Board, Node>();
            openList.Add(startingNode.Board, startingNode);


            while (openList.Count > 0)
            {
                var currentNode = openList[0];
                if (currentNode.Board.Equals(solutionBoard))
                {
                    return ReconstructPath(currentNode);
                }

                openList.Remove(currentNode.Board);
                closedList.Add(currentNode);

                // Resolve possible neighbor moves.
                var neighborNodes = _nodesResolver.ResolveNodes(currentNode, solutionBoard);

                foreach (var neighborNode in neighborNodes)
                {
                    // Skip the neighbor node if it was already evaluated.
                    if (closedList.Contains(neighborNode))
                    {
                        continue;
                    }

                    if (!openList.ContainsKey(neighborNode.Board))
                    {
                        // New node discovered.
                        openList.Add(neighborNode.Board, neighborNode);
                    }
                    else
                    {
                        // Node with such board already in open list.
                        // Check if current neighborNode may have a better g score.
                        if (neighborNode.GScore < openList[neighborNode.Board].GScore)
                        {
                            // Better g score for same board found. Save it.
                            openList[neighborNode.Board] = neighborNode;
                        }
                    }
                }
            }

            return null;
        }

        private IList<Node> ReconstructPath(Node currentNode)
        {
            var nodeList = new List<Node>();

            while (currentNode != null)
            {
                nodeList.Add(currentNode);
                currentNode = currentNode.Parent;
            }

            nodeList.Reverse();

            return nodeList;
        }
    }
}
