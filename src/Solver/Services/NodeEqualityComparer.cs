﻿using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;

namespace SlidingPuzzle.Solver.Services
{
    public class NodeEqualityComparer : IEqualityComparer<Node>
    {
        public bool Equals(Node x, Node y)
        {
            if (x == null)
            {
                throw new ArgumentNullException(nameof(x));
            }

            if (y == null)
            {
                throw new ArgumentNullException(nameof(y));
            }

            return x.Equals(y);
        }

        public int GetHashCode(Node obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return obj.GetHashCode();
        }
    }
}