﻿using SlidingPuzzle.Solver.Heurisitcs.Abstractions;
using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SlidingPuzzle.Solver.Services
{
    public class HeuristicNeighborNodesResolver
    {
        private readonly IReadOnlyList<Direction> _directions;
        private readonly IHeuristicEvaluator _heuristicEvaluator;

        public HeuristicNeighborNodesResolver(IHeuristicEvaluator heuristicEvaluator)
        {
            _directions = typeof(Direction)
                .GetProperties(BindingFlags.Public | BindingFlags.Static)
                .Select(p => p.GetValue(null))
                .OfType<Direction>()
                .ToList()
                .AsReadOnly();
            _heuristicEvaluator = heuristicEvaluator ?? throw new ArgumentNullException(nameof(heuristicEvaluator));
        }

        public IList<Node> ResolveNodes(Node currentNode)
        {
            #region null checks
            if (currentNode == null)
            {
                throw new ArgumentNullException(nameof(currentNode));
            }
            #endregion

            var startingTile = currentNode.Board.EmptyTile;
            var nodeList = new List<Node>();

            foreach (var direction in _directions)
            {
                // Add move translation to create a position for targeted tile.
                var targetPosition = startingTile.Position.AddTranslation(direction.Translation);

                // Try to acquire the target tile.
                // This may fail due to index out of range.
                Tile targetTile = null;
                try
                {
                    targetTile = currentNode.Board[targetPosition.X, targetPosition.Y];
                }
                catch { }

                // Check if can perform the move.
                if (CanMove(startingTile, targetTile))
                {
                    // Swap the tiles in order to create a new board.
                    var postMoveBoard = currentNode.Board.SwapTiles(startingTile, targetTile);

                    var node = new Node(postMoveBoard, currentNode);

                    nodeList.Add(node);
                }
            }

            return nodeList;
        }

        public IList<Node> ResolveNodes(Node currentNode, Board solutionBoard)
        {
            #region null checks
            if (currentNode == null)
            {
                throw new ArgumentNullException(nameof(currentNode));
            }

            if (solutionBoard == null)
            {
                throw new ArgumentNullException(nameof(solutionBoard));
            }
            #endregion

            var startingTile = currentNode.Board.EmptyTile;
            var nodeList = new List<Node>();

            foreach (var direction in _directions)
            {
                // Add move translation to create a position for targeted tile.
                var targetPosition = startingTile.Position.AddTranslation(direction.Translation);

                // Try to acquire the target tile.
                // This may fail due to index out of range.
                Tile targetTile = null;
                try
                {
                    targetTile = currentNode.Board[targetPosition.X, targetPosition.Y];
                }
                catch { }

                // Check if can perform the move.
                if (CanMove(startingTile, targetTile))
                {
                    // Swap the tiles in order to create a new board.
                    var postMoveBoard = currentNode.Board.SwapTiles(startingTile, targetTile);

                    var node = CreateNewNode(postMoveBoard, currentNode, solutionBoard);

                    nodeList.Add(node);
                }
            }

            return nodeList;
        }

        private bool CanMove(Tile startingTile, Tile targetTile)
        {
            return targetTile != null && !targetTile.IsEmpty && startingTile.IsEmpty;
        }

        private Node CreateNewNode(Board postMoveBoard, Node parentNode, Board solutionBoard)
        {
            var node = new Node(postMoveBoard, parentNode);
            node.GScore = parentNode.GScore + 1;
            node.HScore = _heuristicEvaluator.EvaluateDistance(postMoveBoard, solutionBoard);
            node.FScore = node.GScore + node.FScore;

            return node;
        }
    }
}
