﻿using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SlidingPuzzle.Solver.Services
{
    public class NeighborNodesResolver
    {
        private readonly IReadOnlyList<Direction> _directions;

        public NeighborNodesResolver()
        {
            _directions = typeof(Direction)
                .GetProperties(BindingFlags.Public | BindingFlags.Static)
                .Select(p => p.GetValue(null))
                .OfType<Direction>()
                .ToList()
                .AsReadOnly();
        }

        public IList<Node> ResolveNodes(Node currentNode)
        {
            #region null checks
            if (currentNode == null)
            {
                throw new ArgumentNullException(nameof(currentNode));
            }
            #endregion

            var startingTile = currentNode.Board.EmptyTile;
            var nodeList = new List<Node>();

            foreach (var direction in _directions)
            {
                // Add move translation to create a position for targeted tile.
                var targetPosition = startingTile.Position.AddTranslation(direction.Translation);

                // Try to acquire the target tile.
                // This may fail due to index out of range.
                Tile targetTile = null;
                try
                {
                    targetTile = currentNode.Board[targetPosition.X, targetPosition.Y];
                }
                catch { }

                // Check if can perform the move.
                if (CanMove(startingTile, targetTile))
                {
                    // Swap the tiles in order to create a new board.
                    var postMoveBoard = currentNode.Board.SwapTiles(startingTile, targetTile);

                    var node = new Node(postMoveBoard, currentNode);

                    nodeList.Add(node);
                }
            }

            return nodeList;
        }

        private bool CanMove(Tile startingTile, Tile targetTile)
        {
            return targetTile != null && !targetTile.IsEmpty && startingTile.IsEmpty;
        }
    }
}
