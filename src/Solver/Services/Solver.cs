﻿using SlidingPuzzle.Solver.Heurisitcs;
using SlidingPuzzle.Solver.Heurisitcs.Abstractions;
using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace SlidingPuzzle.Solver.Services
{
    /// <summary>
    /// Represents main game object. Controls the game board and the game itself.
    /// </summary>
    public class Solver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Solver"/> class.
        /// </summary>
        public Solver()
        {
            Generator = new BoardGenerator();
            Evaluator = new ManhattanDistanceEvaluator();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Solver"/> class
        /// with heuristic evalutator specified.
        /// </summary>
        /// <param name="heuristicEvaluator">
        /// The heuristic evaluator used.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="heuristicEvaluator"/> is <see langword="null"/>.
        /// </exception>
        public Solver(IHeuristicEvaluator heuristicEvaluator)
        {
            Generator = new BoardGenerator();
            Evaluator = heuristicEvaluator ?? throw new ArgumentNullException(nameof(heuristicEvaluator));
        }

        /// <summary>
        /// Gets the heuristic evalutor currently used by this solver.
        /// </summary>
        public IHeuristicEvaluator Evaluator { get; }

        /// <summary>
        /// Gets the board generator.
        /// </summary>
        public BoardGenerator Generator { get; }

        /// <summary>
        /// Starts the solver and blocks the calling thread until solution is found.
        /// </summary>
        /// <param name="board">
        /// The board with tiles.
        /// </param>
        /// <returns>
        /// A <see cref="IList{T}"/> of <see cref="Node"/>s or <see langword="null"/>
        /// if no solution is found.
        /// </returns>
        public IList<Node> Start(Board board)
        {
            if (board == null)
            {
                throw new ArgumentNullException(nameof(board));
            }

            var solutionBoard = Generator.GenerateSolutionBoard(4, 4);

            var nodeMoveList = new AStar(Evaluator).FindPath(
                startingBoard: board,
                solutionBoard: solutionBoard
            );

            if (nodeMoveList != null)
            {
                return nodeMoveList;
            }
            else
            {
                return null;
            }
        }
    }
}
