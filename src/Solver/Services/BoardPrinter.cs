﻿using SlidingPuzzle.Solver.Models;
using Pastel;
using System.Drawing;
using System.IO;
using System.Linq;

namespace SlidingPuzzle.Solver.Services
{
    public class BoardPrinter
    {
        private readonly TextWriter _textWriter;

        public BoardPrinter(TextWriter textWriter)
        {
            _textWriter = textWriter;
        }

        public void PrintBoard(Board board)
        {
            if (board == null)
            {
                throw new System.ArgumentNullException(nameof(board));
            }

            // Top border.
            _textWriter.Write("╔");
            _textWriter.Write(string.Join("╦", Enumerable.Repeat("════", board.Columns)));
            _textWriter.WriteLine("╗");

            for (int x = 0; x < board.Rows; x++)
            {
                if (x > 0)
                {
                    _textWriter.Write("╠");
                    _textWriter.Write(string.Join("┼", Enumerable.Repeat("────", board.Columns)));
                    _textWriter.WriteLine("╣");
                }

                // Start line with left border.
                _textWriter.Write("║");

                for (int y = 0; y < board.Columns; y++)
                {
                    if (y > 0)
                    {
                        // Table cell seperator.
                        _textWriter.Write("│");
                    }
                    var tile = board[y, x];
                    var tableCellContent = FormatTileContent(tile);
                    _textWriter.Write(tableCellContent);
                }

                // End line with right border.
                _textWriter.WriteLine("║");
            }

            // Bottom border.
            _textWriter.Write("╚");
            _textWriter.Write(string.Join("╩", Enumerable.Repeat("════", board.Columns)));
            _textWriter.WriteLine("╝");
        }

        private string FormatTileContent(Tile tile)
        {
            var contentColor = Color.SeaGreen;
            var content = tile.IsEmpty ? string.Empty : tile.Id.ToString();

            return new string($" {content}   ".Take(4).ToArray()).Pastel(contentColor);
        }
    }
}
