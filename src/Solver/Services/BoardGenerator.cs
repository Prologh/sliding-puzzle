﻿using SlidingPuzzle.Solver.Comparers;
using SlidingPuzzle.Solver.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SlidingPuzzle.Solver.Services
{
    public class BoardGenerator
    {
        private readonly NeighborNodesResolver _neighborNodesResolver;
        private readonly Random _random;

        public BoardGenerator()
        {
            _neighborNodesResolver = new NeighborNodesResolver();
            _random = new Random();
        }

        public Board GenerateRandomBoard(Board solutionBoar, int randomMoves)
        {
            if (solutionBoar == null)
            {
                throw new ArgumentNullException(nameof(solutionBoar));
            }

            if (randomMoves < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(randomMoves));
            }

            var boardComparer = new BoardEqualityComparer();
            var visitedBoards = new HashSet<Board>(comparer: boardComparer);
            var currentBoard = solutionBoar;

            for (int i = 0; i < randomMoves; i++)
            {
                var currentNode = new Node(currentBoard);
                var nodes = _neighborNodesResolver.ResolveNodes(currentNode);

                Node randomNode = null;
                do
                {
                    randomNode = nodes[_random.Next(nodes.Count)];
                } while (visitedBoards.Contains(randomNode.Board, comparer: boardComparer));

                visitedBoards.Add(currentNode.Board);
                //new BoardPrinter(Console.Out).PrintBoard(currentBoard);
                //System.Threading.Thread.Sleep(800);
                //Console.Clear();
                currentBoard = randomNode.Board;
            }

            //new BoardPrinter(Console.Out).PrintBoard(currentBoard);
            //System.Threading.Thread.Sleep(800);
            //Console.Clear();

            return currentBoard;
        }

        public Board GenerateSolutionBoard(int columns, int rows)
        {
            var tileCount = rows * columns;

            var tileArray = new Tile[columns, rows];
            for (int row = 0, id = 1; row < rows; row++)
            {
                for (int column = 0; column < columns; column++, id++)
                {
                    var isEmpty = id == tileCount;
                    var position = new Position(x: column, y: row);
                    var tile = new Tile(id, position, isEmpty);
                    tileArray[column, row] = tile;
                }
            }

            return new Board(tileArray);
        }
    }
}
